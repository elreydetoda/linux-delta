/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package db

import (
	"github.com/go-pg/pg"
	"log"
	"os"
)

type DBOpts interface {
	Addr() string
	User() string
	Pass() string
}

func Connect(dbOpts DBOpts) *pg.DB {
	pgOpts := &pg.Options {
		User: dbOpts.User(),
		Password: dbOpts.Pass(),
		Addr: dbOpts.Addr(),
	}
	db := pg.Connect(pgOpts)
	if db == nil {
		log.Printf("Failed to connect to database.\n")
		os.Exit(100)
	}
	log.Printf("Connection to database successful.\n")
	return db
}

func Close(db *pg.DB) {
	closeErr := db.Close()
	if closeErr != nil {
		log.Printf(
			"Error while closing the connection, Reason: %v\n",
			closeErr,
		)
		os.Exit(100)
	}
	log.Printf("Connection to database closed.\n")
}
