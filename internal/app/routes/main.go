/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package routes

import (
  "fmt"
  "github.com/gorilla/mux"
  sc "github.com/gorilla/securecookie"
  uuid "github.com/satori/go.uuid"
  "gitlab.com/altispeed/linux-delta/internal/app/distros"
  "gitlab.com/altispeed/linux-delta/internal/app/ratings"
  "golang.org/x/crypto/bcrypt"
  "html/template"
  "net/http"
  "strconv"
  "strings"
)

const PathHome = "/"
const PathHomeSort = "/sort"
const PathFavicon = "/favicon.ico"
const PathDistro = "/distro/{distroName:[a-zA-Z0-9\\-_! ]+}"
const PathLogin = "/login"
const PathLogout = "/logout"
const PathRating = "/rating/{ratingId:[a-fA-F0-9\\-]+}"
const PathRatingDelete = PathRating + "/delete"
const PathRatingUseful = PathRating + "/upvote"
const PathDistroRatingSort = PathDistro + "/sort"

const DistroSortRecent = "recent"
const HomeSortAlphabetical = "alphabetical"

type SessionModel struct {
  Id string
  Username string
}
type HomePage struct {
  Session *SessionModel
  Distros []distros.DistroModelView
  Sort string
}
type DistroPage struct {
  Session *SessionModel
  Distro *distros.DistroModel
  RatingsInfo []ratings.RatingsInfoModel
  Ratings []ratings.RatingModel
  Sort string
}
type LoginPage struct {
  Session *SessionModel
  ErrorMsg string
}

var templates *template.Template
var cookieHandler *sc.SecureCookie
var admins map[string]string

func Init(adminList map[string]string) {
  templates = initializeTemplates("resources/templates/")
  cookieHandler = initializeCookieHandler()
  router := mux.NewRouter()
  admins = adminList

  router.HandleFunc(PathHome, homeHandler)
  router.HandleFunc(PathFavicon, faviconHandler)
  router.HandleFunc(PathHomeSort, homeSortHandler).Methods(http.MethodPost)
  router.HandleFunc(PathLogin, loginPageHandler).Methods(http.MethodGet)
  router.HandleFunc(PathLogin, loginFormHandler).Methods(http.MethodPost)
  router.HandleFunc(PathLogout, logoutFormHandler).Methods(http.MethodPost)
  router.HandleFunc(PathDistro, ratingPageHandler).Methods(http.MethodGet)
  router.HandleFunc(PathDistro, ratingFormHandler).Methods(http.MethodPost)
  router.HandleFunc(PathDistro+PathRatingDelete, ratingDeleteHandler)
  router.HandleFunc(PathDistro+PathRatingUseful, ratingUsefulHandler)
  router.HandleFunc(PathDistroRatingSort, ratingSortHandler).
    Methods(http.MethodPost)

  http.Handle("/", router)

  // Static files
  http.Handle(
    "/static/",
    http.StripPrefix(
      "/static/",
      http.FileServer(http.Dir("resources/static"))))
  http.Handle(
    "/.well-known/",
    http.StripPrefix(
      "/.well-known/",
      http.FileServer(http.Dir("resources/well-known"))))
}

func initializeTemplates(tmplRoot string) *template.Template {
  return template.Must(
    template.
      New("main").
      Funcs(
        template.FuncMap {
          "getStarRating": getGetStarRatingFunc(),
          "starCtrl": getStarCtrlFunc(),
          "twoDecFloat": get2DecimalFloatFunc(),
        }).
      ParseFiles(
        tmplRoot + "home_page.html",
        tmplRoot + "distro_page.html",
        tmplRoot + "login_page.html",
      ))
}

func initializeCookieHandler() *sc.SecureCookie {
  // TODO: May need to read in hashKey and blockKey from config in order to
  // persist sessions across restarts
  hashKey := sc.GenerateRandomKey(64)
  blockKey := sc.GenerateRandomKey(32)

  return sc.New(hashKey, blockKey)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
  allDistros, err := distros.GetAll(HomeSortAlphabetical)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  p := &HomePage {
    Session: getSession(r),
    Distros: allDistros,
    Sort: HomeSortAlphabetical,
  }

  renderHomeTemplate(w, p)
}

func homeSortHandler(w http.ResponseWriter, r *http.Request) {
  if err := r.ParseForm(); err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }

  sortOrder := r.FormValue("distro-sort")

  allDistros, err := distros.GetAll(sortOrder)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  p := &HomePage {
    Session: getSession(r),
    Distros: allDistros,
    Sort: sortOrder,
  }

  renderHomeTemplate(w, p)
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
  http.ServeFile(w, r, "./resources/static/images/favicon.ico")
}

func ratingPageHandler(w http.ResponseWriter, r *http.Request) {
  distroName := mux.Vars(r)["distroName"]

  p, err := createDistroPage(distroName, r)

  if  err != nil {
    http.NotFound(w, r)
    return
  }

  renderDistroTemplate(w, p)
}

func ratingSortHandler(w http.ResponseWriter, r *http.Request) {
  distroName := mux.Vars(r)["distroName"]
  if distroName == "" {
    http.NotFound(w, r)
    return
  }

  if err := r.ParseForm(); err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }

  distroId, _ := uuid.FromString(r.FormValue("distroId"))
  sortOrder := r.FormValue("rating-sort")

  distro, err := distros.GetById(distroId)
  if err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }

  ratingsInfo, err := ratings.CalcRatingsInfo(distro.Id)
  if err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }

  distroRatings, err := ratings.GetByDistroId(distro.Id, sortOrder)
  if err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }

  p := &DistroPage {
    Session: getSession(r),
    Distro: distro,
    RatingsInfo: ratingsInfo,
    Ratings: distroRatings,
    Sort: sortOrder,
  }

  renderDistroTemplate(w, p)
}

func ratingFormHandler(w http.ResponseWriter, r *http.Request) {
  distroName := mux.Vars(r)["distroName"]
  if distroName == "" {
    http.NotFound(w, r)
    return
  }

  redirectTarget := fmt.Sprintf("/distro/%s", distroName)

  if err := r.ParseForm(); err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }
  id, _ := uuid.NewV4()
  distroId, _ := uuid.FromString(r.FormValue("distroId"))
  username := r.FormValue("name")
  rateOverall, _ := strconv.ParseFloat(r.FormValue("overall"), 64)
  rateWorkstation, _ := strconv.ParseFloat(r.FormValue("workstation"), 64)
  rateServer, _ := strconv.ParseFloat(r.FormValue("server"), 64)
  rateIoT, _ := strconv.ParseFloat(r.FormValue("iot"), 64)
  comment := r.FormValue("comment")

  rating := ratings.RatingModel{
    Id: id,
    DistroId: distroId,
    Username: username,
    RateOverall: rateOverall,
    RateWorkstation: rateWorkstation,
    RateServer: rateServer,
    RateIoT: rateIoT,
    Comment: comment,
  }

  _, _ = ratings.Save(rating)

  http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func ratingDeleteHandler(w http.ResponseWriter, r *http.Request) {
  sess := getSession(r)
  if sess.Username == "" {
    http.Error(
      w,
      "Unauthorized attempt to delete rating.",
      http.StatusUnauthorized,
    )
    return
  }

  distroName := mux.Vars(r)["distroName"]
  if distroName == "" {
    http.NotFound(w, r)
    return
  }

  redirectTarget := fmt.Sprintf("/distro/%s", distroName)

  ratingId, err := uuid.FromString(mux.Vars(r)["ratingId"])
  if err != nil {
    http.NotFound(w, r)
    return
  }

  _, err = ratings.DeleteById(ratingId)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func ratingUsefulHandler(w http.ResponseWriter, r *http.Request) {
  distroName := mux.Vars(r)["distroName"]
  if distroName == "" {
    http.NotFound(w, r)
    return
  }

  redirectTarget := fmt.Sprintf("/distro/%s", distroName)

  ratingId, err := uuid.FromString(mux.Vars(r)["ratingId"])
  if err != nil {
    http.NotFound(w, r)
    return
  }

  _, err = ratings.MarkUseful(ratingId)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func loginPageHandler(w http.ResponseWriter, r *http.Request) {
  p := &LoginPage{
    Session: getSession(r),
  }

  renderLoginTemplate(w, p)
}

func loginFormHandler(w http.ResponseWriter, r *http.Request) {
  name :=  strings.TrimSpace(r.FormValue("name"))
  unhashedPass := strings.TrimSpace(r.FormValue("pass"))
  hashedPass := admins[name]

  if CheckPasswordHash(unhashedPass, hashedPass) {
    setSession(name, w)
    http.Redirect(w, r, "/", http.StatusFound)
  } else {
    p := &LoginPage{
      ErrorMsg: "Unrecognized username/password combination",
      Session: getSession(r),
    }

    renderLoginTemplate(w, p)
  }
}

func logoutFormHandler(w http.ResponseWriter, r *http.Request) {
  clearSession(w)
  http.Redirect(w, r, "/", http.StatusFound)
}

const EmptyStar = `<i class="star-empty"></i>`
const HalfStar = `<i class="star-half"></i>`
const FullStar = `<i class="star-full"></i>`
const StarRating0 = template.HTML(
    EmptyStar + EmptyStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating0Half = template.HTML(
    HalfStar + EmptyStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating1 = template.HTML(
    FullStar + EmptyStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating1Half = template.HTML(
    FullStar + HalfStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating2 = template.HTML(
    FullStar + FullStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating2Half = template.HTML(
    FullStar + FullStar + HalfStar + EmptyStar + EmptyStar)
const StarRating3 = template.HTML(
    FullStar + FullStar + FullStar + EmptyStar + EmptyStar)
const StarRating3Half = template.HTML(
    FullStar + FullStar + FullStar + HalfStar + EmptyStar)
const StarRating4 = template.HTML(
    FullStar + FullStar + FullStar + FullStar + EmptyStar)
const StarRating4Half = template.HTML(
    FullStar + FullStar + FullStar + FullStar + HalfStar)
const StarRating5 = template.HTML(
    FullStar + FullStar + FullStar + FullStar + FullStar)


func getGetStarRatingFunc() func(rating float64) template.HTML {
  return func(rating float64) template.HTML {
    if rating < 0.5 { return StarRating0 }
    if rating < 1.0 { return StarRating0Half }
    if rating < 1.5 { return StarRating1 }
    if rating < 2.0 { return StarRating1Half }
    if rating < 2.5 { return StarRating2 }
    if rating < 3.0 { return StarRating2Half }
    if rating < 3.5 { return StarRating3 }
    if rating < 4.0 { return StarRating3Half }
    if rating < 4.5 { return StarRating4 }
    if rating < 5.0 { return StarRating4Half }
    return StarRating5
  }
}

func getStarCtrlFunc() func(ctrlName string) template.HTML {
  return func(ctrlName string) template.HTML {
    s5 := starInputAndLabel(ctrlName, "5")
    s4 := starInputAndLabel(ctrlName, "4")
    s3 := starInputAndLabel(ctrlName, "3")
    s2 := starInputAndLabel(ctrlName, "2")
    s1 := starInputAndLabel(ctrlName, "1")
    x := clearStarInputAndLabel(ctrlName)

    return template.HTML(
      `<div class="stars">`+s5+s4+s3+s2+s1+x+`</div>`,
    )
  }
}

func get2DecimalFloatFunc() func(f float64) template.HTML {
  return func(f float64) template.HTML {
    return template.HTML(fmt.Sprintf("%.2f", f))
  }
}

func getSession(r *http.Request) *SessionModel {
  sess := &SessionModel{
    Id: "",
    Username: "",
  }

  if cookie, err := r.Cookie("username"); err == nil {
    cookieValue := make(map[string]string)
    if err = cookieHandler.Decode("username", cookie.Value, &cookieValue); err == nil {
      sess.Id = "00000000-0000-0000-0000-000000000000"
      sess.Username = cookieValue["username"]
    }
  }

  return sess
}

func setSession(username string, w http.ResponseWriter) {
  value := map[string]string {
    "username": username,
  }
  if encoded, err := cookieHandler.Encode("username", value); err == nil {
    cookie := &http.Cookie{
      Name: "username",
      Value: encoded,
      Path: "/",
      MaxAge: 3600,
    }
    http.SetCookie(w, cookie)
  }
}

func clearSession(w http.ResponseWriter) {
  cookie := &http.Cookie{
    Name: "username",
    Value: "",
    Path: "/",
    MaxAge: -1,
  }
  http.SetCookie(w, cookie)
}

func createDistroPage(name string, r *http.Request) (*DistroPage, error) {
  distro, err := distros.GetByName(name)
  if err != nil {
    return nil, err
  }

  ratingsInfo, err := ratings.CalcRatingsInfo(distro.Id)
  if err != nil {
    return nil, err
  }

  distroRatings, err := ratings.GetByDistroId(distro.Id, "")
  if err != nil {
    return nil, err
  }

  return &DistroPage {
    Session:     getSession(r),
    Distro:      distro,
    RatingsInfo: ratingsInfo,
    Ratings:     distroRatings,
    Sort:        DistroSortRecent,
  }, nil
}

func renderHomeTemplate(w http.ResponseWriter, page *HomePage) {
  err := templates.ExecuteTemplate(w, "home_page.html", page)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func renderDistroTemplate(w http.ResponseWriter, page *DistroPage) {
  err := templates.ExecuteTemplate(w, "distro_page.html", page)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func renderLoginTemplate(w http.ResponseWriter, page *LoginPage) {
  err := templates.ExecuteTemplate(w, "login_page.html", page)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func starInputAndLabel(name string, num string) string {
  cn := `"`+`star star-`+num+`" `
  id := `"`+name+`-star-`+num+`" `
  vl := `"`+num+`" `
  nm := `"`+name+`" `

  sb := strings.Builder{}
  sb.WriteString(`<input class=`)
  sb.WriteString(cn)
  sb.WriteString(`id=`)
  sb.WriteString(id)
  sb.WriteString(`type="radio" value=`)
  sb.WriteString(vl)
  sb.WriteString(`name=`)
  sb.WriteString(nm)

  if num == "0" {
    sb.WriteString(`checked `)
  }

  sb.WriteString(`/><label class=`)
  sb.WriteString(cn)
  sb.WriteString(`for=`)
  sb.WriteString(id)
  sb.WriteString(`></label>`)
  return sb.String()
}

func clearStarInputAndLabel(name string) string {
  cn := `"clear-star" `
  id := `"`+name+`-star-0" `
  vl := `"0" `
  nm := `"`+name+`" `

  sb := strings.Builder{}
  sb.WriteString(`<input class=`)
  sb.WriteString(cn)
  sb.WriteString(`id=`)
  sb.WriteString(id)
  sb.WriteString(`type="radio" value=`)
  sb.WriteString(vl)
  sb.WriteString(`name=`)
  sb.WriteString(nm)
  sb.WriteString(`checked `)
  sb.WriteString(`/><label class=`)
  sb.WriteString(cn)
  sb.WriteString(`title="Clear Rating" `)
  sb.WriteString(`for=`)
  sb.WriteString(id)
  sb.WriteString(`></label>`)

  return sb.String()
}

func CheckPasswordHash(password, hash string) bool {
  err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
  return err == nil
}