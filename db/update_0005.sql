---------------------------------------------------------------------------
-- Copyright (C) 2019 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------

-- Add created column to ratings and distros. Default to NOW() to start
-- capturing created data.
ALTER TABLE ratings
    ADD COLUMN created timestamptz NOT NULL DEFAULT NOW();
ALTER TABLE distros
    ADD COLUMN created timestamptz NOT NULL DEFAULT NOW();

-- Set all existing record's created column to Linux Delta's Launch day.
UPDATE ratings SET
    created = '2019-06-17';
UPDATE distros SET
    created = '2019-06-17';

-- Add distros Ubuntu MATE and Guix.
INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
    '2e4cfc29-c379-4f73-9fcd-756a22428a71',
    'Ubuntu MATE',
    'Ubuntu MATE is a stable, easy-to-use operating system with a ' ||
    'configurable desktop environment. It is ideal for those who want the ' ||
    'most out of their computers and prefer a traditional desktop ' ||
    'metaphor. With modest hardware requirements it is suitable for modern ' ||
    'workstations, single board computers and older hardware alike. Ubuntu ' ||
    'MATE makes modern computers fast and old computers usable.',
    'https://ubuntu-mate.org/download/',
    'https://ubuntu-mate.org/',
    'Workstation,Server,IoT,Cloud',
    'ubuntu-mate-vertical.png',
    'ubuntu-mate-vertical.png',
    '#ffffff'
),(
    '79050f94-af77-4ee3-9d46-04d4fedddc0e',
    'Guix',
    'Guix System is a Linux distribution built around the GNU Guix package ' ||
    'manager, itself based on the Nix package manager with Guile Scheme ' ||
    'APIs. Guix enables a declarative operating system configuration and ' ||
    'allows reliable system upgrades that can easily be rolled back.',
    'https://en.wikipedia.org/wiki/GNU_Guix#Guix_System_Distribution',
    'https://guix.gnu.org/',
    'Workstation,Server,IoT,Cloud',
    'guix-horizontal.png',
    'guix-vertical.png',
    '#232323'
);