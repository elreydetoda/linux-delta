---------------------------------------------------------------------------
-- Copyright (C) 2019 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------

INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
  'aa10b860-3bdb-468e-a75a-2dbb55022359',
  'Kubuntu',
  'Kubuntu is an official flavour of the Ubuntu operating system which ' ||
  'uses the KDE Plasma Desktop instead of the GNOME desktop environment. ' ||
  'As part of the Ubuntu project, Kubuntu uses the same underlying ' ||
  'systems. Every package in Kubuntu shares the same repositories as ' ||
  'Ubuntu, and it is released regularly on the same schedule as Ubuntu. ' ||
  'Kubuntu was sponsored by Canonical Ltd. until 2012, and then directly ' ||
  'by Blue Systems. Now employees of Blue Systems contribute upstream, ' ||
  'to KDE and Debian, and Kubuntu development is led by community ' ||
  'contributors.',
  'https://en.wikipedia.org/wiki/Kubuntu',
  'https://kubuntu.org/',
  'Workstation,Server,IoT,Cloud',
  'kubuntu_horizontal.svg',
  'kubuntu_horizontal.svg',
'#ffffff'
),(
  '0ee2ad3d-a3b6-4e0e-b18b-1ee64c892377',
  'Void Linux',
  'Void Linux is an independent Linux distribution that uses the X Binary ' ||
  'Package System (XBPS) package manager, which was designed and ' ||
  'implemented from scratch, and the runit init system. Excluding binary ' ||
  'kernel blobs, a base install is composed entirely of free software, but ' ||
  'users can access an official non-free repository to install proprietary ' ||
  'software.',
  'https://en.wikipedia.org/wiki/Void_Linux',
  'https://voidlinux.org/',
  'Workstation,Server,IoT,Cloud',
  'void-linux-horizontal.png',
  'void-linux-vertical.png',
  '#ffffff'
)