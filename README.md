# Linux Delta

A web application meant to provide a system for users to rate and comment on 
their favorite Linux distributions. Created in partnership with 
[Altispeed Technologies](https://www.altispeed.com/) and the
[Ask Noah Show](https://asknoahshow.com) podcast.

## Requirements
Go v1.11  
PostgreSQL  

## Quick Setup (Currently just notes to self to flesh out later)
- Install Docker
- Download the Postgresql image
- `sudo apt install postgresql-client`
- `psql -h localhost -p 5432 -U postgres -W docker -d postgres -f db/init.sql`